package com.opiniator.myProfile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.opiniator.R
import com.opiniator.adapter.RequestAdapter

class MyProfileRequest : Fragment() {
    lateinit var requestList: RecyclerView
    lateinit var manager: LinearLayoutManager
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v: View = layoutInflater.inflate(R.layout.request, container, false)
        initView(v)
        return v

    }

    fun initView(v: View) {

        requestList = v.findViewById(R.id.requestList)
        manager = LinearLayoutManager(requireContext())
        requestList.layoutManager = manager
        requestList.adapter = RequestAdapter(requireContext())
    }

}