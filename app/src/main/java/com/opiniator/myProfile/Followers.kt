package com.opiniator.myProfile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.opiniator.R
import com.opiniator.adapter.FollowerAdapter

class Followers : Fragment() {

    lateinit var followersList: RecyclerView
    lateinit var manager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v: View = inflater.inflate(R.layout.followers, container, false)
        initView(v)
        return v

    }

    fun initView(v: View) {
        followersList = v.findViewById(R.id.followersList)
        manager = LinearLayoutManager(requireContext())
        followersList.layoutManager = manager
        followersList.adapter = FollowerAdapter(requireContext())

    }

}