package com.opiniator.myProfile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.opiniator.R
import com.opiniator.adapter.CategoryAdapter

class MyProfileCategory : Fragment() {

    lateinit var myProfileCategoryList: RecyclerView
    lateinit var manager: LinearLayoutManager
    var arrayList:ArrayList<String> = ArrayList()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var v: View = inflater.inflate(R.layout.my_profile_category, container, false)
        initView(v)
        return v

    }

    fun initView(v: View) {

        myProfileCategoryList = v.findViewById(R.id.myProfileCategoryList)
        manager = LinearLayoutManager(activity)
        myProfileCategoryList.layoutManager = manager
        myProfileCategoryList.adapter=CategoryAdapter(requireContext(),arrayList)

    }

}