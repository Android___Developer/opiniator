package com.opiniator.myProfile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.opiniator.R
import com.opiniator.adapter.HistoryAdapter

class History : Fragment(), View.OnClickListener {

    lateinit var historyOpinionText: TextView
    lateinit var historyQuestionText: TextView
    lateinit var historyQuestion: RelativeLayout
    lateinit var historyOpinion: RelativeLayout
    lateinit var history_recycler_view: RecyclerView
    lateinit var manager: LinearLayoutManager
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v: View = inflater.inflate(R.layout.history, container, false)
        initView(v)
        return v
    }

    fun initView(v: View) {

        historyOpinionText = v.findViewById(R.id.historyOpinionText)
        historyQuestionText = v.findViewById(R.id.historyQuestionText)
        historyQuestion = v.findViewById(R.id.historyQuestion)
        historyOpinion = v.findViewById(R.id.historyOpinion)
        history_recycler_view = v.findViewById(R.id.history_recycler_view)
        manager = LinearLayoutManager(context)
        history_recycler_view.layoutManager = manager

        historyOpinion.setOnClickListener(this)
        historyQuestion.setOnClickListener(this)
        history_recycler_view.adapter = HistoryAdapter(requireContext(), false)
    }

    override fun onClick(p0: View?) {

        //TODO("not implemented")//To change body of created functions use File | Settings | File Templates.

        when (p0!!.id) {

            R.id.historyOpinion -> {

                historyQuestionText.setTextColor(resources.getColor(R.color.textColor))
                historyOpinionText.setTextColor(resources.getColor(R.color.white))
                historyQuestion.setBackgroundColor(resources.getColor(R.color.white))
                historyOpinion.setBackgroundColor(resources.getColor(R.color.green))
                history_recycler_view.adapter = HistoryAdapter(requireContext(), true)

            }

            R.id.historyQuestion -> {

                historyQuestionText.setTextColor(resources.getColor(R.color.white))
                historyOpinionText.setTextColor(resources.getColor(R.color.textColor))
                historyQuestion.setBackgroundColor(resources.getColor(R.color.green))
                historyOpinion.setBackgroundColor(resources.getColor(R.color.white))
                history_recycler_view.adapter = HistoryAdapter(requireContext(), false)

            }

        }

    }

}