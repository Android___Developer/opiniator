package com.opiniator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView

class NeedHelp : AppCompatActivity(), View.OnClickListener {
    lateinit var toolbarBack: ImageView
    lateinit var toolbarTittle: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.need_help)
        initView()
    }

    fun initView() {
        toolbarBack = findViewById(R.id.toolbarBack)
        toolbarTittle = findViewById(R.id.toolbarTittle)
        toolbarTittle.text = "Need Help"

        toolbarBack.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        when (p0!!.id) {

            R.id.toolbarBack -> {

                onBackPressed()

            }

        }

    }

}