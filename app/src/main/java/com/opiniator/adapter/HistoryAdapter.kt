package com.opiniator.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.opiniator.R

class HistoryAdapter(var context: Context, var value: Boolean) : RecyclerView.Adapter<HistoryAdapter.HistoryHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HistoryHolder {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        var v: View = LayoutInflater.from(context).inflate(R.layout.history_adapter, p0, false)

        return HistoryHolder(v)
    }

    override fun getItemCount(): Int {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return 8
    }

    override fun onBindViewHolder(holder: HistoryHolder, p1: Int) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        if (value) {
            holder.historyAdapterImage.visibility = View.VISIBLE
            holder.historyAdapterImage.setImageDrawable(context.resources.getDrawable(R.drawable.tick_green))
        } else {
            holder.historyAdapterImage.visibility = View.GONE
        }
    }

    inner class HistoryHolder(v: View) : RecyclerView.ViewHolder(v) {

        lateinit var historyAdapterImage: ImageView

        init {

            historyAdapterImage = v.findViewById(R.id.historyAdapterImage)

        }

    }

}