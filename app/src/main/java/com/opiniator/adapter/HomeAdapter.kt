package com.opiniator.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.opiniator.R

class HomeAdapter(internal var list: ArrayList<String>, internal var context: Context) : RecyclerView.Adapter<HomeAdapter.HomeHolder>() {
    lateinit var click: OnHomeAdapterClick
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HomeHolder {
        var v: View = LayoutInflater.from(context).inflate(R.layout.home_adapter, p0, false)
        return HomeHolder(v)
    }

    override fun getItemCount(): Int {
        return 6
    }

    override fun onBindViewHolder(p0: HomeHolder, p1: Int) {

    }

    inner class HomeHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {


        var homeAdapterUserName: TextView
        var homeAdapterFollowers: TextView
        var homeAdapterGender: TextView
        var homeAdapterTime: TextView
        var homeAdapterRankText: TextView
        var homeAdapterQuestion: TextView
        var homeAdapterCategory: TextView
        var homeAdapterLeftQuestionImage: ImageView
        var homeAdapterRightQuestionImage: ImageView

        init {

            homeAdapterUserName = v.findViewById(R.id.homeAdapterUserName)
            homeAdapterFollowers = v.findViewById(R.id.homeAdapterUserName)
            homeAdapterGender = v.findViewById(R.id.homeAdapterGender)
            homeAdapterTime = v.findViewById(R.id.homeAdapterTime)
            homeAdapterRankText = v.findViewById(R.id.homeAdapterRankText)
            homeAdapterQuestion = v.findViewById(R.id.homeAdapterQuestion)
            homeAdapterLeftQuestionImage = v.findViewById(R.id.homeAdapterLeftQuestionImage)
            homeAdapterRightQuestionImage = v.findViewById(R.id.homeAdapterRightQuestionImage)
            homeAdapterCategory = v.findViewById(R.id.homeAdapterCategory)

            homeAdapterLeftQuestionImage.setOnClickListener(this)
            homeAdapterRightQuestionImage.setOnClickListener(this)

        }

        override fun onClick(p0: View?) {

            when (p0!!.id) {

                R.id.homeAdapterLeftQuestionImage -> {

                    click.onHomeQuestionClick(adapterPosition)

                }

                R.id.homeAdapterRightQuestionImage -> {

                    click.onHomeQuestionClick(adapterPosition)

                }

            }

        }

    }

    fun onHomeClick(click: OnHomeAdapterClick) {

        this.click = click

    }

    interface OnHomeAdapterClick {

        fun onHomeQuestionClick(pos: Int)

    }

}