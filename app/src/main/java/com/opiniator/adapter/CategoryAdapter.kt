package com.opiniator.adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.opiniator.R

class CategoryAdapter(internal var context: Context, internal var arrayList: ArrayList<String>) : RecyclerView.Adapter<CategoryAdapter.CategoryHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CategoryHolder {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
       var v:View =LayoutInflater.from(context).inflate(R.layout.category_adapter,p0,false)
        return CategoryHolder(v)
    }

    override fun getItemCount(): Int {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  return 6
    }

    override fun onBindViewHolder(holder: CategoryHolder, p1: Int) {
       // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        if (p1%2==0){
            holder.categoryAdapterCard.setCardBackgroundColor(context.resources.getColor(R.color.green))
            holder.categoryAdapterImage.setImageDrawable(context.resources.getDrawable(R.drawable.tick_white))
        }else{
            holder.categoryAdapterCard.setCardBackgroundColor(context.resources.getColor(R.color.white))
            holder.categoryAdapterImage.setImageDrawable(context.resources.getDrawable(R.drawable.plus))
        }

    }

    inner class CategoryHolder(v: View) : RecyclerView.ViewHolder(v){

        lateinit var categoryAdapterCard: CardView
        lateinit var categoryAdapterText: TextView
        lateinit var categoryAdapterImage: ImageView

        init {
            categoryAdapterCard=v.findViewById(R.id.categoryAdapterCard)
            categoryAdapterText=v.findViewById(R.id.categoryAdapterText)
            categoryAdapterImage=v.findViewById(R.id.categoryAdapterImage)
        }

    }

}