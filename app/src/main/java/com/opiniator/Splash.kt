package com.opiniator

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import com.opiniator.login.Login

class Splash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash)

        val background = object : Thread() {

            override fun run() {

                try {

                    //Thread will sleep for 5 seconds
                    Thread.sleep((2 * 1000).toLong())

                    // After 2 seconds redirect to another intent
                    val i = Intent(this@Splash, Select::class.java)
                    startActivity(i)
                    //Remove activity
                    finish()

                } catch (e: Exception) {

                    e.printStackTrace()

                }

            }

        }
        //start thread
        background.start()

    }
}
