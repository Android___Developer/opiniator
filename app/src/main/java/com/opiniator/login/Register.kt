package com.opiniator.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import com.opiniator.R
import com.opiniator.commonClasses.CommonClasses

class Register : AppCompatActivity() {

    lateinit var alreadyLogin: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register)

        alreadyLogin = findViewById(R.id.alreadyLogin)

        CommonClasses().hideKeyboard(this)
        val ss = SpannableString("Already Have Account? LoginHere")

        val clickableSpan = object : ClickableSpan() {

            override fun onClick(textView: View) {
                val service = Intent(this@Register, Login::class.java)
                startActivity(service)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }

        }

        ss.setSpan(clickableSpan, 22, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        alreadyLogin.setText(ss)
        alreadyLogin.setMovementMethod(LinkMovementMethod.getInstance())
        alreadyLogin.setHighlightColor(resources.getColor(R.color.gradient_start))

    }

}