package com.opiniator.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.opiniator.NeedHelp
import com.opiniator.PrivacyPolicy
import com.opiniator.R
import com.opiniator.commonClasses.CommonClasses
import com.opiniator.dashboard.Home

class Login : AppCompatActivity(), View.OnClickListener {

    lateinit var loginRegister: TextView
    lateinit var forgotPass: TextView
    lateinit var privacyPolicy: TextView
    lateinit var needHelp: TextView
    lateinit var loginButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

       CommonClasses().hideKeyboard(this)

        initView()
    }

    fun initView() {
        loginRegister = findViewById(R.id.loginRegister)
        forgotPass = findViewById(R.id.forgotPass)
        needHelp = findViewById(R.id.needHelp)
        privacyPolicy = findViewById(R.id.privacyPolicy)

        loginButton = findViewById(R.id.loginButton)

        val ss = SpannableString("Don't Have An Account? Register Now")

        val clickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                val service = Intent(this@Login, Register::class.java)
                startActivity(service)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false

            }

        }

        ss.setSpan(clickableSpan, 23, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        loginRegister.setText(ss)
        loginRegister.setMovementMethod(LinkMovementMethod.getInstance())
        loginRegister.setHighlightColor(resources.getColor(R.color.gradient_start))

        forgotPass.setOnClickListener(this)
        privacyPolicy.setOnClickListener(this)
        needHelp.setOnClickListener(this)
        loginButton.setOnClickListener(this)
        loginRegister.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {

        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        when (p0!!.id) {

            R.id.forgotPass -> {

                var forgot = Intent(this@Login, ForgotPass::class.java)
                startActivity(forgot)

            }

            R.id.needHelp -> {

                var forgot = Intent(this@Login, NeedHelp::class.java)
                startActivity(forgot)

            }

            R.id.privacyPolicy -> {

                var forgot = Intent(this@Login, PrivacyPolicy::class.java)
                startActivity(forgot)

            }

            R.id.loginButton -> {

                var home = Intent(this@Login, Home::class.java)
                startActivity(home)

            }

            R.id.loginRegister -> {

                var regi = Intent(this@Login, Register::class.java)
                startActivity(regi)

            }

        }

    }

}