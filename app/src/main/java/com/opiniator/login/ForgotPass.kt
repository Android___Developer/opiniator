package com.opiniator.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.opiniator.R
import com.opiniator.commonClasses.CommonClasses

class ForgotPass : AppCompatActivity() {
    lateinit var forgotRegister: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgot_pass)

        forgotRegister = findViewById(R.id.forgotRegister)
        CommonClasses().hideKeyboard(this)

        forgotRegister.setOnClickListener {
            var regi = Intent(this@ForgotPass, Register::class.java)
            startActivity(regi)
        }

    }

}