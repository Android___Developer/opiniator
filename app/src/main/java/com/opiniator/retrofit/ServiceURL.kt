package com.newendurance.retrofit

object ServiceURL {

    var BASEURL: String = "http://neoendurance.com/api/"

    var LOGIN: String = "login.php"
    var REGISTER: String = "register.php"
    var COUNTRYLSIT: String = "country.php"
    var STATELIST: String = "state.php?country_id="
    var FORGOTPASS: String = "forget.php"
    var CONTACTUS: String = "api_contact.php"
    var RACELIST: String = "races.php"
    var RACEDETAIL: String = "races_search.php?id="
    var FILTERRACELIST: String = "api_filter.php?"
    var CATRACELIST: String = "category.php"
    var BLOGLIST: String = "api_blog.php"
    var BLOGDETAIL: String = "blog_detail.php?id="
    var GALLERYLIST: String = "api_gallery.php"
    var RESULTLIST: String = "api_previous_races.php"

    var IMAGEBASEURL: String = "https://neoendurance.com/media/djcatalog2/images/"
    var RACEIMAGEURL: String = "https://neoendurance.com/media/djcatalog2/images/item/0/"
    var BLOGIMAGEURL: String = "https://neoendurance.com/"

}