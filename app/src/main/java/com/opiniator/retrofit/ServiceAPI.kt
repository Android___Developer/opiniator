package com.newendurance.retrofit

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ServiceAPI {

    @GET
    fun getRequestResponse(@Url url: String): Call<ResponseBody>

    @Multipart
    @POST
    fun getPostResponse(@Url url: String, @PartMap body: HashMap<String, RequestBody>):Call<ResponseBody>
}