package com.newendurance.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitService {

    fun getInitRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(ServiceURL.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

    }

    fun getInitInstance(): ServiceAPI {

        return getInitRetrofit().create(ServiceAPI::class.java)

    }


}