package com.opiniator

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import com.opiniator.login.Login
import com.opiniator.login.Register

class Select : AppCompatActivity(), View.OnClickListener {
    lateinit var selectInfluential: Button
    lateinit var selectOpinion: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.select)
        initView()
    }

    fun initView() {
        selectInfluential = findViewById(R.id.selectInfluential)
        selectOpinion = findViewById(R.id.selectOpinion)
        selectOpinion.setOnClickListener(this)
        selectInfluential.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        when (p0!!.id) {

            R.id.selectOpinion -> {

                var influence = Intent(this@Select, Login::class.java)
                startActivity(influence)

            }

            R.id.selectInfluential -> {

                var influence = Intent(this@Select, Register::class.java)
                startActivity(influence)

            }

        }

    }

}