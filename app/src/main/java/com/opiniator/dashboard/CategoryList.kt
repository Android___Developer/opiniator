package com.opiniator.dashboard

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.opiniator.R
import com.opiniator.adapter.CategoryAdapter

class CategoryList : AppCompatActivity(), View.OnClickListener {

    lateinit var toolbarBackIcon: ImageView
    lateinit var categoryList: RecyclerView
    lateinit var manager: LinearLayoutManager
    var arrayList: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.category_list)
        initView()

    }

    fun initView() {

        toolbarBackIcon = findViewById(R.id.toolbarBackIcon)
        toolbarBackIcon.setOnClickListener(this)
        manager = LinearLayoutManager(this)

        categoryList.layoutManager = manager
        categoryList.adapter = CategoryAdapter(this, arrayList)

    }

    override fun onClick(p0: View?) {

        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        when (p0!!.id) {

            R.id.toolbarBackIcon -> {
                onBackPressed()
            }

        }

    }

}