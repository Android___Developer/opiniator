package com.opiniator.dashboard

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.opiniator.R

open class Dashboard : AppCompatActivity(), View.OnClickListener {

    lateinit var dashboardTop: LinearLayout
    lateinit var container: LinearLayout
    lateinit var home: LinearLayout
    lateinit var opinion: LinearLayout
    lateinit var statics: LinearLayout
    lateinit var setting: LinearLayout
    lateinit var you: LinearLayout

    lateinit var homeImage: ImageView
    lateinit var opinionImage: ImageView
    lateinit var staticsImage: ImageView
    lateinit var settingImage: ImageView
    lateinit var youImage: ImageView

    lateinit var askText: TextView

    lateinit var homeText: TextView
    lateinit var opinionText: TextView
    lateinit var staticsText: TextView
    lateinit var settingText: TextView
    lateinit var youText: TextView

    override fun setContentView(layoutResID: Int) {
        dashboardTop = layoutInflater.inflate(R.layout.dashboard, null) as LinearLayout
        container = dashboardTop.findViewById<View>(R.id.container) as LinearLayout
        layoutInflater.inflate(layoutResID, container, true)
        super.setContentView(dashboardTop)
        initViews()
    }

    fun initViews() {

        home = findViewById(R.id.home)
        opinion = findViewById(R.id.opinion)
        statics = findViewById(R.id.statics)
        setting = findViewById(R.id.setting)
        you = findViewById(R.id.you)

        homeImage = findViewById(R.id.homeImage)
        opinionImage = findViewById(R.id.opinionImage)
        staticsImage = findViewById(R.id.staticsImage)
        settingImage = findViewById(R.id.settingImage)
        youImage = findViewById(R.id.youImage)

        homeText = findViewById(R.id.homeText)
        opinionText = findViewById(R.id.opinionText)
        staticsText = findViewById(R.id.staticsText)
        settingText = findViewById(R.id.settingText)
        youText = findViewById(R.id.youText)

        askText = findViewById(R.id.askText)

        home.setOnClickListener(this)
        opinion.setOnClickListener(this)
        statics.setOnClickListener(this)
        setting.setOnClickListener(this)
        you.setOnClickListener(this)

        askText.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {

        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        when (p0!!.id) {

            R.id.home -> {

                var home = Intent(this@Dashboard, Home::class.java)
                startActivity(home)

            }

            R.id.opinion -> {

                var opinion = Intent(this@Dashboard, Opinions::class.java)
                startActivity(opinion)

            }

            R.id.statics -> {

                var graph = Intent(this@Dashboard, Graph::class.java)
                startActivity(graph)

            }

            R.id.setting -> {

                var setting = Intent(this@Dashboard, Setting::class.java)
                startActivity(setting)

            }

            R.id.you -> {

                var profile = Intent(this@Dashboard, MyProfile::class.java)
                startActivity(profile)

            }

            R.id.askText -> {

                var ask = Intent(this@Dashboard, AskQuestion::class.java)
                startActivity(ask)

            }

        }

    }

}