package com.opiniator.dashboard

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.opiniator.R

class Graph : Dashboard() {
    lateinit var toolbarBackIcon: ImageView
    lateinit var toolbarSearch: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.graph_activity)
        initView()
        setDashboardData()
    }

    fun initView() {
        toolbarBackIcon = findViewById(R.id.toolbarBackIcon)
        toolbarSearch = findViewById(R.id.toolbarSearch)

        toolbarBackIcon.setOnClickListener(this)
    }

    fun setDashboardData() {

        homeImage.setImageDrawable(resources.getDrawable(R.drawable.home))
        opinionImage.setImageDrawable(resources.getDrawable(R.drawable.opinion))
        staticsImage.setImageDrawable(resources.getDrawable(R.drawable.statics_color))
        settingImage.setImageDrawable(resources.getDrawable(R.drawable.settings))
        youImage.setImageDrawable(resources.getDrawable(R.drawable.user))

        homeText.setTextColor(resources.getColor(R.color.black))
        opinionText.setTextColor(resources.getColor(R.color.black))
        staticsText.setTextColor(resources.getColor(R.color.green))
        settingText.setTextColor(resources.getColor(R.color.black))
        youText.setTextColor(resources.getColor(R.color.black))

    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        when (p0!!.id) {
            R.id.toolbarBackIcon -> {
                onBackPressed()
            }
        }
    }


}