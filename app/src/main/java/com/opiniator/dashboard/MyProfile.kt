package com.opiniator.dashboard

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import com.opiniator.R
import com.opiniator.myProfile.*

class MyProfile : Dashboard() {

    lateinit var myProfileRequest: RelativeLayout
    lateinit var myProfileShare: RelativeLayout
    lateinit var myProfileCategory: RelativeLayout
    lateinit var myProfileFollowers: RelativeLayout
    lateinit var myProfileHistory: RelativeLayout
    lateinit var myProfileFrame: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.my_profile)
        initView()
        setDashboardData()

    }

    fun initView() {

        myProfileRequest = findViewById(R.id.myProfileRequest)
        myProfileShare = findViewById(R.id.myProfileShare)
        myProfileCategory = findViewById(R.id.myProfileCategory)
        myProfileFollowers = findViewById(R.id.myProfileFollowers)
        myProfileHistory = findViewById(R.id.myProfileHistory)

        myProfileFrame = findViewById(R.id.myProfileFrame)

        myProfileRequest.setOnClickListener(this)
        myProfileShare.setOnClickListener(this)
        myProfileCategory.setOnClickListener(this)
        myProfileFollowers.setOnClickListener(this)
        myProfileHistory.setOnClickListener(this)

        var fragment: MyProfileAbout = MyProfileAbout()
        replace(fragment)
    }

    override fun onClick(p0: View?) {
        super.onClick(p0)

        when (p0!!.id) {

            R.id.myProfileRequest -> {
                var fragment: MyProfileRequest = MyProfileRequest()
                replace(fragment)
            }

            R.id.myProfileShare -> {

                var share = Intent(Intent.ACTION_SEND)
                share.setType("text/plain")
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
                share.putExtra(Intent.EXTRA_SUBJECT, "Opiniator")
                share.putExtra(Intent.EXTRA_TEXT, "this is the body of the text")
                startActivity(Intent.createChooser(share, "Share Opiniator"))

            }

            R.id.myProfileCategory -> {
                var fragment: MyProfileCategory = MyProfileCategory()
                replace(fragment)
            }

            R.id.myProfileFollowers -> {
                var fragment: Followers = Followers()
                replace(fragment)
            }

            R.id.myProfileHistory -> {
                var fragment: History = History()
                replace(fragment)
            }

        }

    }

    fun replace(fragment: Fragment) {

        var fragmentManager: FragmentManager = supportFragmentManager
        var transation: FragmentTransaction = fragmentManager.beginTransaction()
        transation.replace(R.id.myProfileFrame, fragment).commit()

    }

    fun setDashboardData() {

        homeImage.setImageDrawable(resources.getDrawable(R.drawable.home))
        opinionImage.setImageDrawable(resources.getDrawable(R.drawable.opinion))
        staticsImage.setImageDrawable(resources.getDrawable(R.drawable.statics))
        settingImage.setImageDrawable(resources.getDrawable(R.drawable.settings))
        youImage.setImageDrawable(resources.getDrawable(R.drawable.user_color))

        homeText.setTextColor(resources.getColor(R.color.black))
        opinionText.setTextColor(resources.getColor(R.color.black))
        staticsText.setTextColor(resources.getColor(R.color.black))
        settingText.setTextColor(resources.getColor(R.color.black))
        youText.setTextColor(resources.getColor(R.color.green))

    }

}