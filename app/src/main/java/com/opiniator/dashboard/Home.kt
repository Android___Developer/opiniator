package com.opiniator.dashboard

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import com.opiniator.R
import com.opiniator.adapter.HomeAdapter

class Home : Dashboard(), View.OnClickListener {

    lateinit var questionsList: RecyclerView
    lateinit var manager: RecyclerView.LayoutManager
    var arraList: ArrayList<String> = ArrayList()
    lateinit var toolbarBackIcon: ImageView
    lateinit var adapter: HomeAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.questions_window)
        initView()
    }

    fun initView() {

        questionsList = findViewById(R.id.questionsList)
        toolbarBackIcon = findViewById(R.id.toolbarBackIcon)
        manager = LinearLayoutManager(this)

        questionsList.layoutManager = manager
        adapter = HomeAdapter(arraList, this)
        questionsList.adapter = adapter

        adapter.onHomeClick(object : HomeAdapter.OnHomeAdapterClick {
            override fun onHomeQuestionClick(pos: Int) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                val homeAlertSubmitButton: TextView
                //type ==0 for success,1 for error
                val dialog = Dialog(this@Home)
                dialog.setContentView(R.layout.question_alert)
                homeAlertSubmitButton = dialog.findViewById(R.id.homeAlertSubmitButton)
                dialog.setCancelable(false)

                dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                val params = dialog.window!!.attributes
                params.gravity = Gravity.CENTER_VERTICAL
                //todo for remove the animation
                //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                homeAlertSubmitButton.setOnClickListener { dialog.cancel() }

                dialog.show()

            }

        })

        setDashboardData()
        toolbarBackIcon.setOnClickListener(this)
    }

    fun setDashboardData() {

        homeImage.setImageDrawable(resources.getDrawable(R.drawable.home_color))
        opinionImage.setImageDrawable(resources.getDrawable(R.drawable.opinion))
        staticsImage.setImageDrawable(resources.getDrawable(R.drawable.statics))
        settingImage.setImageDrawable(resources.getDrawable(R.drawable.settings))
        youImage.setImageDrawable(resources.getDrawable(R.drawable.user))

        homeText.setTextColor(resources.getColor(R.color.green))
        opinionText.setTextColor(resources.getColor(R.color.black))
        staticsText.setTextColor(resources.getColor(R.color.black))
        settingText.setTextColor(resources.getColor(R.color.black))
        youText.setTextColor(resources.getColor(R.color.black))

    }

    override fun onClick(p0: View?) {
        super.onClick(p0)

        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        when (p0!!.id) {

            R.id.toolbarBackIcon -> {

                onBackPressed()

            }


        }

    }

}