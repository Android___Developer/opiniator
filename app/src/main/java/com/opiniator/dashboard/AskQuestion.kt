package com.opiniator.dashboard

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.opiniator.R

class AskQuestion : AppCompatActivity(), View.OnClickListener {

    lateinit var toolbarBackIcon: ImageView
    lateinit var askQuestionViewCategory: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ask_question)

        initView()
    }

    fun initView() {
        toolbarBackIcon = findViewById(R.id.toolbarBackIcon)
        askQuestionViewCategory = findViewById(R.id.askQuestionViewCategory)


        toolbarBackIcon.setOnClickListener(this)
        askQuestionViewCategory.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {

        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        when (p0!!.id) {

            R.id.toolbarBackIcon -> {
                onBackPressed()
            }

            R.id.askQuestionViewCategory -> {

                var catList = Intent(this@AskQuestion, CategoryList::class.java)
                startActivity(catList)

            }

        }

    }

}