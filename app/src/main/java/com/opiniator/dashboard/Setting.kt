package com.opiniator.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.opiniator.PrivacyPolicy
import com.opiniator.R
import com.opiniator.login.Login

class Setting : Dashboard(), View.OnClickListener {

    lateinit var toolbarBackIcon: ImageView
    lateinit var toolbarSearch: ImageView

    lateinit var editLinear: LinearLayout
    lateinit var changePassword: LinearLayout
    lateinit var aboutLinear: LinearLayout
    lateinit var contactLinear: LinearLayout
    lateinit var privacyLinear: LinearLayout
    lateinit var logoutLinear: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.setting)
        initView()
    }

    fun initView() {

        toolbarBackIcon = findViewById(R.id.toolbarBackIcon)
        toolbarSearch = findViewById(R.id.toolbarSearch)

        editLinear = findViewById(R.id.editLinear)
        changePassword = findViewById(R.id.changePassword)
        aboutLinear = findViewById(R.id.aboutLinear)
        contactLinear = findViewById(R.id.contactLinear)
        privacyLinear = findViewById(R.id.privacyLinear)
        logoutLinear = findViewById(R.id.logoutLinear)

        editLinear.setOnClickListener(this)
        changePassword.setOnClickListener(this)
        aboutLinear.setOnClickListener(this)
        contactLinear.setOnClickListener(this)
        privacyLinear.setOnClickListener(this)
        logoutLinear.setOnClickListener(this)
        toolbarBackIcon.setOnClickListener(this)

        setDashboardData()

    }

    fun setDashboardData() {

        homeImage.setImageDrawable(resources.getDrawable(R.drawable.home))
        opinionImage.setImageDrawable(resources.getDrawable(R.drawable.opinion))
        staticsImage.setImageDrawable(resources.getDrawable(R.drawable.statics))
        settingImage.setImageDrawable(resources.getDrawable(R.drawable.settings_color))
        youImage.setImageDrawable(resources.getDrawable(R.drawable.user))

        homeText.setTextColor(resources.getColor(R.color.black))
        opinionText.setTextColor(resources.getColor(R.color.black))
        staticsText.setTextColor(resources.getColor(R.color.black))
        settingText.setTextColor(resources.getColor(R.color.green))
        youText.setTextColor(resources.getColor(R.color.black))

    }

    override fun onClick(p0: View?) {
        super.onClick(p0)
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        when (p0!!.id) {

            R.id.toolbarBackIcon -> {

                onBackPressed()

            }

            R.id.editLinear -> {

                var editProfile = Intent(this@Setting, EditProfile::class.java)
                startActivity(editProfile)

            }

            R.id.changePassword -> {

                var changePassword = Intent(this@Setting, ChangePassword::class.java)
                startActivity(changePassword)

            }

            R.id.aboutLinear -> {

                var about = Intent(this@Setting, WhoWe::class.java)
                startActivity(about)
            }

            R.id.contactLinear -> {

                var contactUs=Intent(this@Setting,ContactUs::class.java)
                startActivity(contactUs)

            }

            R.id.privacyLinear -> {

                var privacyPolicy = Intent(this@Setting, AccountPrivacy::class.java)
                startActivity(privacyPolicy)

            }

            R.id.logoutLinear -> {

                var login = Intent(this@Setting, Login::class.java)
                startActivity(login)

            }

        }

    }

}