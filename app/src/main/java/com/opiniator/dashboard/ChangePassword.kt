package com.opiniator.dashboard

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.opiniator.R

class ChangePassword :AppCompatActivity(), View.OnClickListener {
    lateinit var toolbarBack: ImageView
    lateinit var toolbarTittle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_password)
        initView()
    }

    fun initView() {
        toolbarBack = findViewById(R.id.toolbarBack)
        toolbarTittle = findViewById(R.id.toolbarTittle)

        toolbarTittle.text = "Change Password"
        toolbarBack.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {

        when (p0!!.id) {

            R.id.toolbarBack -> {

                onBackPressed()

            }

        }

    }

}