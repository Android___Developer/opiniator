package com.opiniator.dashboard

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.opiniator.R

class Opinions : Dashboard() {
    lateinit var toolbarBackIcon: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.opinions)

        initView()
    }

    fun initView() {
        toolbarBackIcon = findViewById(R.id.toolbarBackIcon)

        toolbarBackIcon.setOnClickListener(this)
        setDashboardData()
    }

    fun setDashboardData() {

        homeImage.setImageDrawable(resources.getDrawable(R.drawable.home))
        opinionImage.setImageDrawable(resources.getDrawable(R.drawable.opinion_color))
        staticsImage.setImageDrawable(resources.getDrawable(R.drawable.statics))
        settingImage.setImageDrawable(resources.getDrawable(R.drawable.settings))
        youImage.setImageDrawable(resources.getDrawable(R.drawable.user))

        homeText.setTextColor(resources.getColor(R.color.black))
        opinionText.setTextColor(resources.getColor(R.color.green))
        staticsText.setTextColor(resources.getColor(R.color.black))
        settingText.setTextColor(resources.getColor(R.color.black))
        youText.setTextColor(resources.getColor(R.color.black))

    }
    override fun onClick(p0: View?) {
        super.onClick(p0)
        when (p0!!.id) {

            R.id.toolbarBackIcon -> {
                onBackPressed()
            }
        }
    }

}