package com.opiniator.dashboard

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import com.opiniator.R

class WhoWe : AppCompatActivity(), View.OnClickListener {


    lateinit var toolbarBackIcon: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.who_we)
        initView()
    }

    fun initView() {
        toolbarBackIcon = findViewById(R.id.toolbarBackIcon)


        toolbarBackIcon.setOnClickListener(this)
    }


    override fun onClick(p0: View?) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        when (p0!!.id) {
            R.id.toolbarBackIcon -> {

                onBackPressed()
            }
        }
    }
}